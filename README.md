### Exploratory High-Dimensional Time Series (XHiDiTS) toolbox ###

* The XHiDiTS toolbox contains a comprehensive collection of methods and tools that can be used to study the properties of high dimensional time series, including the connectivity (coherence, partial coherence, partial directed coherence, block coherence, ...), spectrum (auto-spectrum, cross-spectrum) and the factors that summarize the highly correlated time series. 

* The toolbox also provides a user-friendly GUI that allows user to interactively explore the time series data recorded for multiple trials, from multiple subjects, under different conditions.

![Screen Shot 2016-04-14 at 2.17.09 PM.png](https://bitbucket.org/repo/bk6o47/images/1207550240-Screen%20Shot%202016-04-14%20at%202.17.09%20PM.png)


![Screen Shot 2016-04-14 at 7.01.08 PM.png](https://bitbucket.org/repo/bk6o47/images/862269218-Screen%20Shot%202016-04-14%20at%207.01.08%20PM.png)

* Version 0.2

### How do I get set up? ###
* Requirement

    Matlab software

* Configuration

    Set the working directory to the home directory of this toolbox


    Run XHDTS()

* Development Instructions

    The folder ./Utilities contains utility functions that help compute the results in the toolbox.

    XHDTS.fig contains the design of the GUI (open using GUIDE)

    XHDTS.m contains all the callback functions for the objects on the graphical panel


### Who do I talk to? ###