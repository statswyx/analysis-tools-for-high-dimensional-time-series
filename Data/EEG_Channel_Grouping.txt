% Prefrontal region
left_Pf=[32 37 46 54 61 33 38 47 55 27 34 39 48 28 35];
right_Pf=[1 2 3 4 10 11 12 13 18 19 20 25 220 221 222];

% Dorsolateral prefrontal region
left_dPf=[40 48 47 39 35];
right_dPf=[223 222 2 3 4];

% Premotor region
left_PMd=[57 56 49 50 41 42 43 36 30];
right_PMd=[204 212 205 213 197 206 214 215 224];

% Supplementary motor area
SMA=[6 15 23 207 7 16 24 198 8 17];

% Anterior SMA
aSMA=[5 14 22 29 6 15 23];
% Posterior SMA
pSMA=[24 16 7 207 17 8 198];

% Primary motor region
left_M1=[59 58 51 52 60 66 65];
right_M1=[155 164 182 183 184 195 196];

% Parietal region
left_Pr=[89 77 76 88 87 86 85 100 99 98 97 96 110 109 108 107 106 118];
right_Pr=[130 163 172 142 153 162 171 129 141 152 161 170 128 140 151 160 169 127];

% Lateral Parietal region
left_latPr=[76 85 96 106 77 86 97 107];
right_latPr=[163 162 161 160 172 171 170 169];

% Medial Parietal region
left_medPr=setdiff(left_Pr,left_latPr);
right_medPr=setdiff(right_Pr,right_latPr);

% Anterior parietal region
left_antPr=[76 77 87 88 89];
right_antPr=[163 172 130 142 153];