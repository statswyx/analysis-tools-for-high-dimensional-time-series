% compute block coherence between regions through factors
clear;clc;close all;
% load data
trial = 1;
EEG = get_EEG_data('_all_', trial);
load('../Data/chan_group.mat');
region_names = {'SMA', 'left_Pf', 'right_latPr'};
set(0, 'DefaulttextInterpreter', 'none');


% compute factors
n_factors = 2;
f_time_all = [];
for indx_region = 1:length(region_names)
    region_name = region_names{indx_region};
    chan_indx = chan_group(region_name);
    X_time = EEG(:, chan_indx);
    [f_time, lambda, var_accounted] = factor_spectrum_domain(X_time, n_factors);
    f_time_all = [f_time_all f_time];
end

block_name = region_names;
block_indx = {[1 2], [3 4], [5 6]};
[block_coh, f] = block_coherence(f_time_all, block_indx, block_name);

% display
freq_2_display = [2, 4, 6, 12, 24];
i = 0;
fig = figure();

for freq = freq_2_display
    i = i + 1;
    subplot(3, 2, i);
    %set(gca,'TickLabelInterpreter','latex')

    freq_indx = freq + 1;
    imagesc(block_coh(:, :, freq_indx), [0 1]);
    title([num2str(freq) ' Hz']);
    colorbar;
    colormap jet;
    %set(gca, 'DefaulttextInterpreter', 'none');
    set(gca, 'YTick', 1:3, 'YTickLabel', block_name);
    set(gca, 'XTick', 1:3, 'XTickLabel', block_name);
    %set( get( gca, 'XLabel' ), 'Interpreter', 'none' );


end



