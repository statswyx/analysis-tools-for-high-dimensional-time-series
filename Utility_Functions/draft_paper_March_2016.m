load('Data/chan_group.mat');
%EEG_data = get_eeg_from_subject_name('LARB', 'rest');
region_names = {'SMA', 'left_Pf', 'SMA', 'SMA', 'SMA', 'SMA', 'SMA'};
trial_index = 1;
plot_eeg_by_region(EEG_data, region_names, trial_index);

%% test: get_EEG_by_region
EEGdata = get_eeg_from_subject_name('LARB', 'rest');
load('Data/chan_group.mat');

% EEGdata is T by num_channel by num_trial
trial_index = 1;
region_names = {'SMA', 'left_Pf'};

EEG = get_EEG_by_region(region_names, EEGdata, trial_index, chan_group);

Fs = 1000;
nfft = 1000;
X_time = EEG;
[Sxx, ~, f] = multi_psd(X_time, nfft, Fs);
Sxx(isnan(Sxx)) = 0;
Coh = zeros(size(Sxx));
for i = 1:size(Sxx, 3)
    diag_mat = diag(sqrt(diag(Sxx(:, :, i)).^(-1)));
    Coh(:, :, i) = diag_mat * Sxx(:, :, i) * diag_mat;
end
Coh = abs(Coh).^2;

%% text color
figure;
annotation('textbox', 'Position', [0 0 .15 .15], 'String', ['p = ' num2str(333)], 'Color', 'b' );
annotation('textbox', 'Position', [0 0 .25 .15], 'String', ['p = ' num2str(44444)], 'Color', 'g' );
