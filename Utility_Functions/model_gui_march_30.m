function varargout = model_gui_march_30(varargin)
% MODEL_GUI_MARCH_30 MATLAB code for model_gui_march_30.fig
%      MODEL_GUI_MARCH_30, by itself, creates a new MODEL_GUI_MARCH_30 or raises the existing
%      singleton*.
%
%      H = MODEL_GUI_MARCH_30 returns the handle to a new MODEL_GUI_MARCH_30 or the handle to
%      the existing singleton*.
%
%      MODEL_GUI_MARCH_30('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MODEL_GUI_MARCH_30.M with the given input arguments.
%
%      MODEL_GUI_MARCH_30('Property','Value',...) creates a new MODEL_GUI_MARCH_30 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before model_gui_march_30_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to model_gui_march_30_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help model_gui_march_30

% Last Modified by GUIDE v2.5 09-Apr-2016 23:09:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @model_gui_march_30_OpeningFcn, ...
                   'gui_OutputFcn',  @model_gui_march_30_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before model_gui_march_30 is made visible.
function model_gui_march_30_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to model_gui_march_30 (see VARARGIN)

% Choose default command line output for model_gui_march_30
handles.output = hObject;
handles.user_data = struct(...
    'data_dir', './Data', ...
    'chan_group', [], ...
    'eeg_data', [], ...
    'T', [], ...
    'num_trials', [], ...
    'subject_name', [], ...
    'subject_condition', [], ...
    'num_channels', [], ...
    'spectrum_matrix', [], ...
    'squared_coherence_matrix', [], ...
    'frequency', [], ... 
    'num_factors', [], ...
    'proprtion_var', [] ...
    );
load(fullfile(handles.user_data.data_dir, 'chan_group.mat'));

% set channel group
handles.user_data.chan_group =  chan_group;  

% set num_factors
contents = cellstr(get(handles.popupmenu_num_factors,'String'));
num_factors = str2num(contents{get(handles.popupmenu_num_factors,'Value')});
handles.user_data.num_factors = num_factors;

% update eeg data
update_eeg_data(hObject, eventdata, handles);


% UIWAIT makes model_gui_march_30 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = model_gui_march_30_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu_subject_name.
function popupmenu_subject_name_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_subject_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_subject_name contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_subject_name
% get subject_name
contents = cellstr(get(handles.popupmenu_subject_name,'String'));
subject_name = contents{get(handles.popupmenu_subject_name,'Value')};

% types of subject_condition depends on subject_name
subject_condition_options = [];

if strcmp(subject_name, 'Simulated')
    subject_condition_options = {''};
else
    subject_condition_options = {'rest', 'test1', 'test2', 'test3'};
end
set(handles.popupmenu_subject_condition, 'String', subject_condition_options);

update_eeg_data(hObject, eventdata, handles);
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);


function popupmenu_subject_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_subject_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
    %set(hOjbect, 'Text', {'LARB', 'BLAK', 'Simulated'});
end

% --- Executes on selection change in listbox_brain_region.
function listbox_brain_region_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_brain_region (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_brain_region contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_brain_region

set(hObject,'Max',2,'Min',0);
contents = cellstr(get(hObject,'String'));
selected_regions = contents(get(hObject,'Value'));

axes(handles.axes_2d_eeg_display);
cla;
plot_channel_regions(selected_regions);
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);
cla(handles.axes_spectrum_plot);


% --- Executes during object creation, after setting all properties.
function listbox_brain_region_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_brain_region (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

load('Data/chan_group.mat');
set(hObject, 'String', chan_group.keys());
%set(hObject, 'String', 'aaa');

% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1



% --- Executes on slider movement.
function slider_trial_index_Callback(hObject, eventdata, handles)
% hObject    handle to slider_trial_index (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
slider_value = get(hObject,'Value');
hObject.Value=round(slider_value);
set(handles.edit1,'String',num2str(hObject.Value));
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function slider_trial_index_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_trial_index (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
edit1_val = str2double(get(hObject,'String'));
set(handles.slider_trial_index, 'Value', edit1_val);
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes_2d_eeg_display_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_2d_eeg_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_2d_eeg_display


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2


% --- Executes during object creation, after setting all properties.
function uipanel3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object deletion, before destroying properties.
function uipanel3_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to uipanel3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_num_of_factors_Callback(hObject, eventdata, handles)
% hObject    handle to edit_num_of_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_num_of_factors as text
%        str2double(get(hObject,'String')) returns contents of edit_num_of_factors as a double


% --- Executes during object creation, after setting all properties.
function edit_num_of_factors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_num_of_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_proportion_of_var_Callback(hObject, eventdata, handles)
% hObject    handle to edit_proportion_of_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_proportion_of_var as text
%        str2double(get(hObject,'String')) returns contents of edit_proportion_of_var as a double


% --- Executes during object creation, after setting all properties.
function edit_proportion_of_var_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_proportion_of_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_subject_condition.
function popupmenu_subject_condition_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_subject_condition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_subject_condition contents as cell array
%        contents{get(hObject,'Value')} returns selected item from
%        popupmenu_subject_condition

contents = cellstr(get(hObject,'String'));
subject_condition = contents{get(hObject,'Value')};
handles.user_data.subject_condition = subject_condition;
guidata(hObject, handles);

update_eeg_data(hObject, eventdata, handles);
disp(handles.user_data.subject_condition);
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_subject_condition_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_subject_condition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes during object creation, after setting all properties.
function uipanel_factor_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_factor_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


%%% - SELF DEFINED FUNCTION ---------
% --- plot EEG signals for given regions
function plot_eeg_signals_uipanel3(hObject, eventdata, handles)
% get trial index
handles = guidata(hObject);
slider_value = round(get(handles.slider_trial_index,'Value'));
trial_index = slider_value;

% get selected region form listbox_brain_region
contents = cellstr(get(handles.listbox_brain_region,'String'));
region_names = contents(get(handles.listbox_brain_region,'Value'));

% plot
delete(allchild(handles.uipanel3));
plot_eeg_by_region(handles.user_data.eeg_data, region_names, trial_index, handles.uipanel3);

% ----- update EEG data when the subject_name or condition has changed
function update_eeg_data(hObject, eventdata, handles)
% get subject_name
contents = cellstr(get(handles.popupmenu_subject_name,'String'));
subject_name = contents{get(handles.popupmenu_subject_name,'Value')};
handles.user_data.subject_name = subject_name;

% get condition
contents = cellstr(get(handles.popupmenu_subject_condition,'String'));
condition = contents{get(handles.popupmenu_subject_condition,'Value')};
handles.user_data.eeg_data = get_eeg_from_subject_name(subject_name, condition);

disp(['EEG data updated to Subject: ' subject_name ', condition: ' condition]);
% Update handles structure
guidata(hObject, handles);

% ------ update plot factor activity on panel 
function plot_factor_activity_on_panel(hObject, eventdata, handles)
handles = guidata(hObject);
% get trial index
slider_value = round(get(handles.slider_trial_index,'Value'));
trial_index = slider_value;

% get selected region form listbox_brain_region
contents = cellstr(get(handles.listbox_brain_region,'String'));
region_names = contents(get(handles.listbox_brain_region,'Value'));

% get number of factors 
nfactors = handles.user_data.num_factors;

% --- plot ------
% get panel handle 
panel_handle = handles.uipanel_factor_plot;
delete(allchild(panel_handle));

% load color map
load('color_map_region.mat');

% channel group 
chan_group = handles.user_data.chan_group;

% layout
max_rows = 5;
num_rows = min(max_rows, length(region_names));
num_columns = ceil(length(region_names)/max_rows);

% transpose subplots so that it fills first column first
subplot_index = reshape(1:num_rows*num_columns, num_columns,[])';
for i  = 1:length(region_names)
    region_name = region_names{i};
    channel_index = chan_group(region_name);
    regional_signal = handles.user_data.eeg_data(:, channel_index, trial_index);
    subplot(num_rows, num_columns, subplot_index(i), 'Parent', panel_handle);
    %plot(regional_signal);
    [f_time, lambda, var_accounted] = factor_spectrum_domain(regional_signal, nfactors);
    plot(f_time, 'LineWidth', 1.5);
    %ylim([-30, 30]);
    title(region_name, 'Color', color_map_region(region_name), 'Interpreter', 'none');
    set(gca, 'XTickLabel', []);
    axis tight;
end


% --- Executes on selection change in popupmenu_connectivity_measure.
function popupmenu_connectivity_measure_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_connectivity_measure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_connectivity_measure contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_connectivity_measure


% --- Executes during object creation, after setting all properties.
function popupmenu_connectivity_measure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_connectivity_measure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_get_frequency_Callback(hObject, eventdata, handles)
% hObject    handle to slider_get_frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

slider_value = get(hObject,'Value');
hObject.Value=round(slider_value);
set(handles.edit_frequency,'String',num2str(hObject.Value));
plot_spectrum_on_axes(hObject, eventdata, handles);
plot_squared_coherence_on_axes(hObject, eventdata, handles);



% --- Executes during object creation, after setting all properties.
function slider_get_frequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_get_frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit_frequency_Callback(hObject, eventdata, handles)
% hObject    handle to edit_frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_frequency as text
%        str2double(get(hObject,'String')) returns contents of edit_frequency as a double

edit_val = str2double(get(hObject,'String'));
set(handles.slider_get_frequency, 'Value', edit_val);
plot_spectrum_on_axes(hObject, eventdata, handles);
plot_squared_coherence_on_axes(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit_frequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_option_spectrum_eeg.
function popupmenu_option_spectrum_eeg_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_option_spectrum_eeg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_option_spectrum_eeg contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_option_spectrum_eeg


% --- Executes during object creation, after setting all properties.
function popupmenu_option_spectrum_eeg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_option_spectrum_eeg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function uibuttongroup2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uibuttongroup2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function uipanel6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ---- self defined function
function compute_spectrum(hObject, eventdata, handles)
% compute spectrum matrix and store the spectrum matrix in
% handles.user_data.spectrum_matrix
% handles.user_data.frequency

% get trial_indx
slider_value = round(get(handles.slider_trial_index,'Value'));
trial_index = slider_value;

% get selected regions form listbox_brain_region
contents = cellstr(get(handles.listbox_brain_region,'String'));
region_names = contents(get(handles.listbox_brain_region,'Value'));

% load chan_group
% get EEG
EEG = get_EEG_by_region(region_names, ...
    handles.user_data.eeg_data, ... 
    trial_index, ... 
    handles.user_data.chan_group);

%disp(size(EEG));
% compute spectrum
nfft = 1000;
Fs = 1000;
[Sxx, ~, f] = multi_psd(EEG, nfft, Fs);

% save computed spectrum matrix Sxx
handles.user_data.spectrum_matrix = Sxx;
handles.user_data.frequency = f;

% compute squared coherence
Sxx(isnan(Sxx)) = 0;
Coh = zeros(size(Sxx));
for i = 1:size(Sxx, 3)
    diag_mat = diag(sqrt(diag(Sxx(:, :, i)).^(-1)));
    Coh(:, :, i) = diag_mat * Sxx(:, :, i) * diag_mat;
end
squared_coherence_matrix = abs(Coh).^2;

% save squared coherence matrix
handles.user_data.squared_coherence_matrix = squared_coherence_matrix;

% save updates
guidata(hObject, handles);



function plot_spectrum_on_axes(hObject, eventdata, handles)
handles = guidata(hObject);

% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_spectrum_plot);
cla;
imagesc(abs(handles.user_data.spectrum_matrix(:, :, frequency_index)));
set(gca,'XTickLabel',[], 'YTickLabel', []);

function plot_squared_coherence_on_axes(hObject, eventdata, handles)
handles = guidata(hObject);
% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_dependence_metric);
cla;
imagesc(handles.user_data.squared_coherence_matrix(:, :, frequency_index), ...
    [0 1]);
set(gca,'XTickLabel',[], 'YTickLabel', []);

% --- Executes on button press in pushbutton_compute_spectrum.
function pushbutton_compute_spectrum_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_compute_spectrum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% compute the spectrum
compute_spectrum(hObject, eventdata, handles);
% update the plot
plot_spectrum_on_axes(hObject, eventdata, handles);
plot_squared_coherence_on_axes(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function axes_spectrum_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_spectrum_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_spectrum_plot


% --- Executes on key press with focus on slider_trial_index and none of its controls.
function slider_trial_index_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to slider_trial_index (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on listbox_brain_region and none of its controls.
function listbox_brain_region_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to listbox_brain_region (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu_num_factors.
function popupmenu_num_factors_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_num_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_num_factors contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_num_factors
contents = cellstr(get(hObject,'String'));
num_factors = str2num(contents{get(hObject,'Value')});
handles.user_data.num_factors = num_factors;
guidata(hObject, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);



% --- Executes during object creation, after setting all properties.
function popupmenu_num_factors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_num_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_prop_var.
function popupmenu_prop_var_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_prop_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_prop_var contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_prop_var


% --- Executes during object creation, after setting all properties.
function popupmenu_prop_var_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_prop_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu10.
function popupmenu10_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu10 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu10


% --- Executes during object creation, after setting all properties.
function popupmenu10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over text_channel_2d_display.
function text_channel_2d_display_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to text_channel_2d_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function axes_dependence_metric_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_dependence_metric (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_dependence_metric
