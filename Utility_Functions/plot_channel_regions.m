function plot_channel_regions(region_names, with_channel_number)
if nargin < 2
    with_channel_number = false;
end


working_dir = './';
% load EEG data
% num_channel by num_obs by num_epoch
%data_dir = [working_dir 'Data/' SUBJECT_NAME '/' SUBJECT_NAME '_' CONDITION '.mat'];
%data = importdata(data_dir);
%data = permute(data, [3 2 1]);

% load channel information
% load channel location
load(fullfile(working_dir, '/Data/egihc256redhm.mat'));
load(fullfile(working_dir, '/Data/chan_group.mat'));
chan_used = EGIHC256RED.ChansUsed;
chan_loc = nan(256, 2);
chan_loc(chan_used, :) = EGIHC256RED.Electrode.Coord2D;

% ------------------------
% region used in the plot
% ------------------------
load(fullfile(working_dir,'/Data/Regions.mat'));
%region_used = Regions;
region_used = region_names;
count = 1;
group = [];
loc_x = [];
loc_y = [];
for indx_region = 1:length(region_used)
    r = region_used{indx_region};
    chans = chan_group(r);
    for chan = chans
         tmp = chan_loc(chan, :);
         loc_x(end+1) = tmp(1);
         loc_y(end+1) = tmp(2);
         group(end+1) = indx_region;
    end
end
%

if with_channel_number
% visualize SMA region
%fig = figure('visible','off');
%fig = figure('Visible','Off');
scatter(chan_loc(chan_used, 1), chan_loc(chan_used, 2), 10);
hold on;
%cmap_all_region = hsv(length(chan_keys()));
load('color_map_region.mat');
cmap = zeros(length(region_names), 3);
for i = 1:length(region_names)
    cmap(i, :) = color_map_region(region_names{i});
end


ax = gscatter(loc_x, loc_y, group, cmap, [], 10);
legend(ax, region_used ,'Location', 'northeastoutside', 'Interpreter', 'none')

%scatter(chan_loc(chan_group('SMA'), 1), chan_loc(chan_group('SMA'), 2), 'red');
%legend(['', 'SMA region']);
% draw a toy head

% add location text
%chans = [23 51 164 172 77];
chans = 1:256;
x = chan_loc(chans, 1);
y = chan_loc(chans, 2);
dx = 0.00;
dy = 0.00;
c = cellstr(num2str(chans'));
text(x+dx, y+dy, c);

%
theta = linspace(0, 2*pi, 1000);
x = 0.95*cos(theta);
y = 0.95*sin(theta)-0.05;
plot(x, y, 'b');
axis equal;
axis off;
hold off;
%fig_name = [working_dir '/Figure/B_regions_visualize_with_channel_number'];
%print(fig, fig_name ,'-depsc2','-r300');

else


% visulize without numbering
        
% visualize SMA region
%fig = figure('visible','off');
%fig = figure('Visible','Off');
scatter(chan_loc(chan_used, 1), chan_loc(chan_used, 2), 30);
hold on;

load('color_map_region.mat');
cmap = zeros(length(region_names), 3);
for i = 1:length(region_names)
    cmap(i, :) = color_map_region(region_names{i});
end
ax = gscatter(loc_x, loc_y, group, cmap, [], 30);
legend(ax, region_used ,'Location', 'northeastoutside', 'Interpreter', 'none')
legend off;
%scatter(chan_loc(chan_group('SMA'), 1), chan_loc(chan_group('SMA'), 2), 'red');
%legend(['', 'SMA region']);
% draw a toy head

% add location text
%chans = [23 51 164 172 77];
% chans = 1:256;
% x = chan_loc(chans, 1);
% y = chan_loc(chans, 2);
% dx = 0.00;
% dy = 0.00;
% c = cellstr(num2str(chans'));
% text(x+dx, y+dy, c);

%
theta = linspace(0, 2*pi, 1000);
x = 0.95*cos(theta);
y = 0.95*sin(theta)-0.05;
plot(x, y, 'b');
axis equal;
axis off;
hold off;
%fig_name = [working_dir '/Figure/B_regions_visualize_without_number'];
%print(fig, fig_name ,'-depsc2','-r300');
end

