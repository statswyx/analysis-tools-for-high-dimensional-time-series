function [Sxx, X_freq, f] = multi_psd(X_time, nfft, Fs)

% load data
%{
SUBJECT_NAME = 'LARB';
CONDITION = 'REST1';
working_dir = '/Users/yxwang/Google Drive/Research/Paper/Paper_2/Factor_Analysis_Spectrum_Domain/';
% load EEG data
% num_channel by num_obs by num_epoch
data_dir = [working_dir 'Data/' SUBJECT_NAME '/' SUBJECT_NAME '_' CONDITION '.mat'];
data = importdata(data_dir);
data = permute(data, [3 2 1]);


X_time = data(1:20, :, 1)';
%}

[T, P] = size(X_time);

if nargin < 2
    nfft = T;
    Fs = 1000;
elseif nargin < 3
    nfft = T;
end


% X is T_by_P
% fourier transformation of X, denoted as X_freq(k, :)
X_freq = fft(X_time, nfft, 1);


% compute periodogram of X, denoted as I(:, :, k)
% I(k) = X_freq(k, :)' * X_freq(k, :)
I = zeros(P, P, nfft);
for k = 1:nfft
    I(:, :, k) = X_freq(k, :)' * X_freq(k, :);
end

%

% compute the spectrum density estimation average
a = 1;
m = 5;
h = repmat(1/(2*m + 1), [2*m+1 1]);

% pad
I_padded = cat(3, I(:, :, (nfft-(m-1)):nfft), I, I(:, :, 1:m));

Sxx = filter(h,a,I_padded, [], 3);
Sxx = Sxx(:, :, (2*m+1):(end));

f = (0:(nfft-1))/nfft * Fs;
end