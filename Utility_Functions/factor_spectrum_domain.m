function [f_time, lambda, var_accounted] = factor_spectrum_domain(X_time, nfactors)

if nargin < 2
    nfactors = 2;
    warning('num_factors is smaller than 2, use 2 instead');
end

if nfactors > size(X_time, 2)
    nfactors = size(X_time,2);
    warning(['num_factors is larger than the number of channels, use all ' ...
        num2str(nfactors) ' channels']);
end


% load data
%{
SUBJECT_NAME = 'LARB';
CONDITION = 'REST1';
working_dir = '/Users/yxwang/Google Drive/Research/Paper/Paper_2/Factor_Analysis_Spectrum_Domain/';
% load EEG data
% num_channel by num_obs by num_epoch
data_dir = [working_dir 'Data/' SUBJECT_NAME '/' SUBJECT_NAME '_' CONDITION '.mat'];
data = importdata(data_dir);
data = permute(data, [3 2 1]);


X_time = data(1:20, :, 1)';
%}

[T, P] = size(X_time);
nfft = T;
Fs = 1000;
[Sxx, X_freq, dummy_f] = multi_psd(X_time, nfft, Fs);

% PCA in freq domain
% nfactors = 2;
var_accounted = zeros(nfft, nfactors);
lambda = zeros(nfft, nfactors);
C_freq = zeros(nfft, P, nfactors);

for k = 1:(floor(nfft/2) + 1)
    [V lambdas] = eig(Sxx(:, :, k));
    [lambdas order] = sort(diag(lambdas),'descend');  %# sort eigenvalues in descending order
    V = V(:,order);
    lambdas_cumsum = cumsum(lambdas);
    lambdas_sum = sum(lambdas);
    var_accounted(k, :) = lambdas_cumsum(1:nfactors)/lambdas_sum;
    for indx_factor = 1:nfactors
        C_freq(k, :, indx_factor) = V(:,indx_factor);
    end
    lambda(k, :) = lambdas(1:nfactors);
    
end

for k = (floor(nfft/2) + 2):nfft
    var_accounted(k, :) = var_accounted(nfft+2-k, :);
    C_freq(k, :, :) = conj(C_freq(nfft+2-k, :, :));
    lambda(k, :) = lambda(nfft+2-k, :);
end


% compute factor activity in freq domain
f_freq = zeros(nfft, nfactors);
%f_time_real = zeros(T, nfactors);
%f_time_imag = zeros(T, nfactors);
f_time = zeros(T, nfactors);
for indx_factor = 1:nfactors
    %{
    for k = 1:nfft
    f_freq(k, indx_factor) = dot(C_freq(end-k+1, :, indx_factor), X_freq(k, :));
    end
    %}
    %C_freq_old = C_freq;
    %for indx_freq = 2:T
    %    C_freq(indx_freq, :, :) = C_freq_old(T+2-indx_freq, :, :);
    %end
    
    %f_freq(:, indx_factor) = sum(conj(C_freq(:, :, indx_factor)) .* X_freq, 2);
    f_freq(:, indx_factor) = sum(C_freq(:, :, indx_factor) .* X_freq, 2);

    tmp = ifft(f_freq(:, indx_factor), T);
    tmp = conj(tmp);
    f_time(:, indx_factor) = real(tmp);
    if max(abs(imag(tmp(:)))) > 1e-9
        error(['Non-zero imaginary part!!! Max(abs(imag(result))) = ' num2str(max(abs(imag(tmp(:))))) ...
            '; Max(abs(real(result))) = ' num2str(max(abs(real(tmp(:)))))]);
    end
end

end
