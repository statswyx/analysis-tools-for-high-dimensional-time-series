function mat_out = matrix_add_margin(mat, start_end, margin, fill_with)
% add margin to matrix so that each region are separated by 
% 0 with width = margin

if nargin == 2
    margin = 2;
    fill_with = 0;
elseif nargin == 3
    fill_with = 0;
end


num_blocks = size(start_end, 1);

i_start = zeros(1, num_blocks);
i_end = zeros(1, num_blocks);
% compute all start locations
i_start(1) = 1;

for i = 2:num_blocks
    i_end(i-1) = i_start(i-1) + start_end(i-1, 2) - start_end(i-1, 1);
    i_start(i) = i_end(i-1) + margin;
end

i_end(num_blocks) = i_start(num_blocks) + start_end(num_blocks, 2) - start_end(num_blocks, 1);

mat_out = zeros(max(i_end));
mat_out(:) = fill_with;

for i = 1:num_blocks
    for j = 1:num_blocks
        mat_out(i_start(i):i_end(i), i_start(j):i_end(j)) = ...
            mat(start_end(i, 1): start_end(i, 2), start_end(j, 1):start_end(j, 2));
    end
end
