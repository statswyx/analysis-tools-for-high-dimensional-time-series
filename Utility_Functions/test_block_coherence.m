clear;clc;close all;

%% test 1
load('../Data/chan_group.mat');
X_time = get_EEG_data('_all_', 1);

block_name = {'SMA', 'left_Pf'};
block_indx = cell(size(block_name));

for i = 1:length(block_name)
    block_indx{i} = chan_group(block_name{i});
end

tic;
[b_coh, f] = block_coherence(X_time, block_indx, block_name);
toc

%% test 2
block_name = {'regoin1', 'region2'};
block_indx = {[1 2], [3 4 ]};
X_time = normrnd(0, 1, [10000, 6]);
tic;
[b_coh, f] = block_coherence(X_time, block_indx, block_name);
toc
