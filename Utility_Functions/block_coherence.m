function [b_coh, f] = block_coherence(X_time, block_indx, block_name)
% compute the block coherence between blocks of signals
% example: block_name = {'sma', 'left_pf', ...};
% block_indx = {[1 2 3 4], [5, 6, 7, 8, 9], [10, 11]};
% return nd_array with dimension (n_block, n_block, n_freq)
% default configuration

T = size(X_time, 1);
nfft = T;
Fs = 1000;
[Sxx, X_freq, f] = multi_psd(X_time, nfft, Fs);

n_block = length(block_indx);
n_freq = length(f);

b_coh = zeros(n_block, n_block, n_freq);

for indx_freq = 1:n_freq
    for i = 1:n_block
        indx_ii = block_indx{i};
        Sii = det(Sxx(indx_ii, indx_ii, indx_freq));
        for j = 1:n_block
            indx_jj = block_indx{j};
            Sjj = det(Sxx(indx_jj, indx_jj, indx_freq));
            
            indx_ij = [indx_ii indx_jj];
            Sij = det(Sxx(indx_ij, indx_ij, indx_freq));
            

            b_coh(i, j, indx_freq) = 1 - Sij / (Sii * Sjj);
        end
    end
end

if max(abs(imag(b_coh(:)))) > 1e-9
    error('wrong!');
end


b_coh = real(b_coh);
end





