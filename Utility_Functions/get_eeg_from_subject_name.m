function eeg_data = get_eeg_from_subject_name(subject_name, condition)
% load eeg data form given subject name and condition (rest/test, etc)
% return a multi-variate array


data_dir = './Data';
condition2name = containers.Map({'rest', 'test1', 'test2', 'test3'}, ...
    {'REST1', 'T1', 'T2', 'T3'});

file_name = strcat(subject_name, '_', condition2name(condition), '.mat');
file_loc = fullfile(data_dir, subject_name, file_name);
S = load(file_loc);
field_names = fieldnames(S);
eeg_data = getfield(S, field_names{1});
eeg_data = permute(eeg_data, [2, 3, 1]);

end


