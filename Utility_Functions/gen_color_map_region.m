function gen_color_map_region()
% this function generate the colormap for the regions
load('Data/chan_group.mat');
region_names = chan_group.keys();
color_map = hsv(length(region_names));
cmaps = containers.Map();
for i = 1:length(region_names)
    cmaps(region_names{i}) = color_map(i, :);
end
color_map_region = cmaps;
save('color_map_region.mat', 'color_map_region');
end

