function EEG = get_EEG_by_region(region_names, EEGdata, trial_index, chan_group)
% get eeg signals by region
% the gap between regions is set to be nan

EEG = [];
T = size(EEGdata, 1);
for i = 1:length(region_names)
    region_name = region_names{i};
    chan_index = chan_group(region_name);
    EEG = [EEG EEGdata(:, chan_index, trial_index)];
end


