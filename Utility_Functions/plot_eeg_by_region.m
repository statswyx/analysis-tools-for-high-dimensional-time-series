function plot_eeg_by_region(EEG_data, region_names, trial, panel_handle)
% plto eeg data for given region names and trial
% region_names: cell array, e.g. {'SMA', 'left_Pf'}
% trial: integer, e.g. 1
% EEG_data is T * num_channels * num_trials

%%
% load channel group information
working_dir = '/Users/yxwang/Google Drive/Research/Paper/Paper_2/Exploratory_Data_Analysis/';
S = load(fullfile(working_dir, 'Data', 'chan_group.mat'));
chan_group = S.chan_group;

% load color map
load('color_map_region.mat');

% layout
max_rows = 5;
num_rows = min(max_rows, length(region_names));
num_columns = ceil(length(region_names)/max_rows);

% plot

% transpose subplots so that it fills first column first
subplot_index = reshape(1:num_rows*num_columns, num_columns,[])';

for i  = 1:length(region_names)
    region_name = region_names{i};
    channel_index = chan_group(region_name);
    regional_signal = EEG_data(:, channel_index, trial);
    subplot(num_rows, num_columns, subplot_index(i), 'Parent', panel_handle);
    plot(regional_signal);
    %ylim([-30, 30]);
    title(region_name, 'Color', color_map_region(region_name), 'Interpreter', 'none');
    set(gca, 'XTickLabel', []);
    axis tight;
end

end

