function varargout = XHDTS(varargin)
% XHDTS MATLAB code for XHDTS.fig
%      XHDTS, by itself, creates a new XHDTS or raises the existing
%      singleton*.
%
%      H = XHDTS returns the handle to a new XHDTS or the handle to
%      the existing singleton*.
%
%      XHDTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in XHDTS.M with the given input arguments.
%
%      XHDTS('Property','Value',...) creates a new XHDTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before XHDTS_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to XHDTS_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help XHDTS

% Last Modified by GUIDE v2.5 12-Apr-2016 02:12:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @XHDTS_OpeningFcn, ...
                   'gui_OutputFcn',  @XHDTS_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before XHDTS is made visible.
function XHDTS_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to XHDTS (see VARARGIN)

% Choose default command line output for XHDTS
handles.output = hObject;

% add path for utility_functions
addpath('./Utility_Functions/');

% user_data
handles.user_data = struct(...
    'data_dir', './Data', ...
    'chan_group', [], ...
    'eeg_data', [], ...
    'T', [], ...
    'num_trials', [], ...
    'subject_name', [], ...
    'subject_condition', [], ...
    'trial_index', [], ...
    'num_channels', [], ...
    'spectrum_matrix', [], ...
    'coherence_matrix', [], ...
    'block_coherence', [], ...
    'partial_coherence_matrix', [], ...
    'frequency', [], ... 
    'num_factors', [], ...
    'proprtion_var', [], ...
    'dependency_metric', [], ...
    'selected_region_start_end', [], ...
    'selected_region_names', [], ...
    'margin_width', 3, ...
    'eeg_auto_spectrum', [], ...
    'factor_auto_spectrum', [], ...
    'factor_cross_spectrum', [], ...
    'factor_time_series', [], ...
    'margin_width_factors', 2, ...
    'factor_start_end', [], ...
    'factor_coherence', [], ...
    'factor_partial_coherence', [] ...
    );

% initializaion the user_data
load(fullfile(handles.user_data.data_dir, 'chan_group.mat'));

% set channel group
handles.user_data.chan_group =  chan_group;  

% set num_factors
contents = cellstr(get(handles.popupmenu_num_factors,'String'));
num_factors = str2num(contents{get(handles.popupmenu_num_factors,'Value')});
handles.user_data.num_factors = num_factors;

% set trial index
slider_value = get(handles.slider_trial_index,'Value');
handles.user_data.trial_index = round(slider_value);

% update eeg data
update_eeg_data(hObject, eventdata, handles);
handles = guidata(hObject);
plot_factor_activity_on_panel(hObject, eventdata, handles);


%listbox_brain_region_Callback(hObject, eventdata, handles);

% UIWAIT makes XHDTS wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = XHDTS_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu_subject_name.
function popupmenu_subject_name_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_subject_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_subject_name contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_subject_name
% get subject_name
contents = cellstr(get(handles.popupmenu_subject_name,'String'));
subject_name = contents{get(handles.popupmenu_subject_name,'Value')};

% types of subject_condition depends on subject_name
subject_condition_options = [];

if strcmp(subject_name, 'Simulated')
    subject_condition_options = {''};
else
    subject_condition_options = {'rest', 'test1', 'test2', 'test3'};
end
set(handles.popupmenu_subject_condition, 'String', subject_condition_options);

update_eeg_data(hObject, eventdata, handles);
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);


function popupmenu_subject_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_subject_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
    %set(hOjbect, 'Text', {'LARB', 'BLAK', 'Simulated'});
end

% --- Executes on selection change in listbox_brain_region.
function listbox_brain_region_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_brain_region (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_brain_region contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_brain_region

set(hObject,'Max',2,'Min',0);
contents = cellstr(get(hObject,'String'));
selected_regions = contents(get(hObject,'Value'));

% update user_data
handles.user_data.selected_region_names = selected_regions;

% update the selected_region_start_end
i_start = 0;
i_end = 0;
region_start_end = zeros(length(selected_regions), 2);
for i = 1:length(selected_regions)
    i_start = i_end + 1;
    i_end = i_start + length(handles.user_data.chan_group(selected_regions{i})) - 1;
    region_start_end(i, :) = [i_start i_end];
end

handles.user_data.selected_region_start_end = region_start_end;


% update data
guidata(hObject, handles);
disp('done updating selected regions');

axes(handles.axes_2d_eeg_display);
cla;
plot_channel_regions(selected_regions);

handles = guidata(hObject);
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);
pushbutton_compute_spectrum_Callback(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function listbox_brain_region_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_brain_region (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

load('Data/chan_group.mat');
set(hObject, 'String', chan_group.keys());
%set(hObject, 'String', 'aaa');

% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1



% --- Executes on slider movement.
function slider_trial_index_Callback(hObject, eventdata, handles)
% hObject    handle to slider_trial_index (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
slider_value = get(hObject,'Value');
hObject.Value=round(slider_value);
handles.user_data.trial_index = round(slider_value);
guidata(hObject, handles);
set(handles.edit1,'String',num2str(hObject.Value));
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);
pushbutton_compute_spectrum_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function slider_trial_index_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_trial_index (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
edit1_val = str2double(get(hObject,'String'));
set(handles.slider_trial_index, 'Value', edit1_val);
handles.user_data.trial_index = edit1_val;
guidata(hObject, handles);

plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);
pushbutton_compute_spectrum_Callback(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function axes_2d_eeg_display_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_2d_eeg_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_2d_eeg_display


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2


% --- Executes during object creation, after setting all properties.
function uipanel3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object deletion, before destroying properties.
function uipanel3_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to uipanel3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_num_of_factors_Callback(hObject, eventdata, handles)
% hObject    handle to edit_num_of_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_num_of_factors as text
%        str2double(get(hObject,'String')) returns contents of edit_num_of_factors as a double


% --- Executes during object creation, after setting all properties.
function edit_num_of_factors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_num_of_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_proportion_of_var_Callback(hObject, eventdata, handles)
% hObject    handle to edit_proportion_of_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_proportion_of_var as text
%        str2double(get(hObject,'String')) returns contents of edit_proportion_of_var as a double


% --- Executes during object creation, after setting all properties.
function edit_proportion_of_var_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_proportion_of_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_subject_condition.
function popupmenu_subject_condition_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_subject_condition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_subject_condition contents as cell array
%        contents{get(hObject,'Value')} returns selected item from
%        popupmenu_subject_condition

contents = cellstr(get(hObject,'String'));
subject_condition = contents{get(hObject,'Value')};
handles.user_data.subject_condition = subject_condition;
guidata(hObject, handles);
update_eeg_data(hObject, eventdata, handles);
disp(handles.user_data.subject_condition);
handles = guidata(hObject);
plot_eeg_signals_uipanel3(hObject, eventdata, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);
handles = guidata(hObject);
pushbutton_compute_spectrum_Callback(hObject, eventdata, handles);
guidata(hObject, handles);
% --- Executes during object creation, after setting all properties.
function popupmenu_subject_condition_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_subject_condition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes during object creation, after setting all properties.
function uipanel_factor_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_factor_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


%%% - SELF DEFINED FUNCTION ---------
% --- plot EEG signals for given regions
function plot_eeg_signals_uipanel3(hObject, eventdata, handles)
% get trial index
handles = guidata(hObject);
slider_value = round(get(handles.slider_trial_index,'Value'));
trial_index = slider_value;

% get selected region form listbox_brain_region
contents = cellstr(get(handles.listbox_brain_region,'String'));
region_names = contents(get(handles.listbox_brain_region,'Value'));

% plot
delete(allchild(handles.uipanel3));
plot_eeg_by_region(handles.user_data.eeg_data, region_names, trial_index, handles.uipanel3);

% ----- update EEG data when the subject_name or condition has changed
function update_eeg_data(hObject, eventdata, handles)
% get subject_name
contents = cellstr(get(handles.popupmenu_subject_name,'String'));
subject_name = contents{get(handles.popupmenu_subject_name,'Value')};
handles.user_data.subject_name = subject_name;

% get condition
contents = cellstr(get(handles.popupmenu_subject_condition,'String'));
condition = contents{get(handles.popupmenu_subject_condition,'Value')};
handles.user_data.eeg_data = get_eeg_from_subject_name(subject_name, condition);

disp(['EEG data updated to Subject: ' subject_name ', condition: ' condition]);
% Update handles structure
guidata(hObject, handles);
% use updated handles
%listbox_brain_region_Callback(hObject, eventdata, handles);
% handles = guidata(hObject);

% ------ update plot factor activity on panel 
function plot_factor_activity_on_panel(hObject, eventdata, handles)
% handles = guidata(hObject);
% get trial index
slider_value = round(get(handles.slider_trial_index,'Value'));
trial_index = slider_value;

% get selected region form listbox_brain_region
contents = cellstr(get(handles.listbox_brain_region,'String'));
region_names = contents(get(handles.listbox_brain_region,'Value'));

% get number of factors 
nfactors = handles.user_data.num_factors;

% --- plot ------
% get panel handle 
panel_handle = handles.uipanel_factor_plot;
delete(allchild(panel_handle));

% load color map
load('color_map_region.mat');

% channel group 
chan_group = handles.user_data.chan_group;

% layout
max_rows = 5;
num_rows = min(max_rows, length(region_names));
num_columns = ceil(length(region_names)/max_rows);

% factor activity (concatenate all)
factor_time_series = [];
factor_start_end = zeros(length(region_names), 2);
factor_start_end(1, 1) = 1;

% factor_
% transpose subplots so that it fills first column first
subplot_index = reshape(1:num_rows*num_columns, num_columns,[])';
for i  = 1:length(region_names)
    region_name = region_names{i};
    channel_index = chan_group(region_name);
    regional_signal = handles.user_data.eeg_data(:, channel_index, trial_index);
    subplot(num_rows, num_columns, subplot_index(i), 'Parent', panel_handle);
    %plot(regional_signal);
    [f_time, lambda, var_accounted] = factor_spectrum_domain(regional_signal, nfactors);
    factor_time_series = [factor_time_series f_time];
    if i < length(region_names)
        factor_start_end(i, 2) = factor_start_end(i, 1) + size(f_time, 2) - 1;
        factor_start_end(i+1, 1) = factor_start_end(i, 2) + 1;
    else
        factor_start_end(i, 2) = factor_start_end(i, 1) + size(f_time, 2) - 1;
    end
        
    
    plot(f_time, 'LineWidth', 1.5);
    %ylim([-30, 30]);
    title(region_name, 'Color', color_map_region(region_name), 'Interpreter', 'none');
    set(gca, 'XTickLabel', []);
    axis tight;
end

% save to user data
handles.user_data.factor_time_series = factor_time_series;
handles.user_data.factor_start_end = factor_start_end;

% update handles
guidata(hObject, handles);

% display
disp('done updating factor time series');
% disp(size(handles.user_data.factor_time_series));


% --- Executes on selection change in popupmenu_connectivity_measure.
function popupmenu_connectivity_measure_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_connectivity_measure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_connectivity_measure contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_connectivity_measure
% compute the spectrum
update_spectrum_and_connectivity_matrix(hObject, eventdata, handles);
% update the plot
update_spectrum_and_connectivity_plot(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function popupmenu_connectivity_measure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_connectivity_measure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_get_frequency_Callback(hObject, eventdata, handles)
% hObject    handle to slider_get_frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

slider_value = get(hObject,'Value');
hObject.Value=round(slider_value);
set(handles.edit_frequency,'String',num2str(hObject.Value));
%plot_eeg_spectrum(hObject, eventdata, handles);
%plot_coherence_on_axes(hObject, eventdata, handles);
update_spectrum_and_connectivity_plot(hObject, eventdata, handles);



% --- Executes during object creation, after setting all properties.
function slider_get_frequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_get_frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit_frequency_Callback(hObject, eventdata, handles)
% hObject    handle to edit_frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_frequency as text
%        str2double(get(hObject,'String')) returns contents of edit_frequency as a double

edit_val = str2double(get(hObject,'String'));
set(handles.slider_get_frequency, 'Value', edit_val);
update_spectrum_and_connectivity_plot(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit_frequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_frequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_option_spectrum_eeg.
function popupmenu_option_spectrum_eeg_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_option_spectrum_eeg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_option_spectrum_eeg contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_option_spectrum_eeg
update_spectrum_and_connectivity_matrix(hObject, eventdata, handles);
update_spectrum_and_connectivity_plot(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_option_spectrum_eeg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_option_spectrum_eeg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function uibuttongroup2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uibuttongroup2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function uipanel6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ---- self defined function
% - get selected eeg data
function EEG = get_selected_eeg_data(hObject, eventdata, handles)
% handles = guidata(hObject);
start_end = handles.user_data.selected_region_start_end;
T = size(handles.user_data.eeg_data, 1);
EEG = zeros(T, max(start_end(:)));
num_selected_regions = length(handles.user_data.selected_region_names);
for i_region = 1:num_selected_regions
    region_name = handles.user_data.selected_region_names{i_region};
    chan_index = handles.user_data.chan_group(region_name);
    EEG(1:T, start_end(i_region, 1):start_end(i_region, 2)) = ...
        handles.user_data.eeg_data(:, chan_index, handles.user_data.trial_index);
end


% - compute factor cross spectrum
function compute_factor_cross_spectrum(hObject, eventdata, handles)
% compute spectrum matrix and store the spectrum matrix in
% handles.user_data.spectrum_matrix
% handles.user_data.frequency
% handles = guidata(hObject);
% disp(size(EEG));
% compute spectrum
nfft = 1000;
Fs = 1000;
[Sxx, ~, f] = multi_psd(handles.user_data.factor_time_series, nfft, Fs);
% save computed spectrum matrix Sxx
handles.user_data.factor_cross_spectrum = Sxx;
handles.user_data.frequency = f;

% update user_data
guidata(hObject, handles);

% display
disp('done computing factor cross spectrum');

% - compute spectrum
function compute_spectrum(hObject, eventdata, handles)
% compute spectrum matrix and store the spectrum matrix in
% handles.user_data.spectrum_matrix
% handles.user_data.frequency

% get trial_indx
slider_value = round(get(handles.slider_trial_index,'Value'));
trial_index = slider_value;

% get selected regions form listbox_brain_region
contents = cellstr(get(handles.listbox_brain_region,'String'));
region_names = contents(get(handles.listbox_brain_region,'Value'));

% load chan_group
% get EEG
EEG = get_selected_eeg_data(hObject, eventdata, handles);

%disp(size(EEG));
% compute spectrum
nfft = 1000;
Fs = 1000;
[Sxx, ~, f] = multi_psd(EEG, nfft, Fs);

% save computed spectrum matrix Sxx
handles.user_data.spectrum_matrix = Sxx;
handles.user_data.frequency = f;

% update user_data
guidata(hObject, handles);


% - compute factor auto spectrum
function compute_factor_auto_spectrum(hObject, eventdata, handles)
handles = guidata(hObject);

% get eeg cross spectrum
Sxx = handles.user_data.factor_cross_spectrum;

% get eeg auto spectrum
auto_spectrum = zeros(size(Sxx,1), size(Sxx, 3)); 
for i = 1:size(Sxx, 1)
    auto_spectrum(i, :) = squeeze(Sxx(i, i, :));
end

% save computed auto spectrum
handles.user_data.factor_auto_spectrum = auto_spectrum;

% save data
guidata(hObject, handles);



% - compute eeg auto spectrum
function compute_eeg_auto_spectrum(hObject, eventdata, handles)
handles = guidata(hObject);

% get eeg cross spectrum
Sxx = handles.user_data.spectrum_matrix;

% get eeg auto spectrum
auto_spectrum = zeros(size(Sxx,1), size(Sxx, 3)); 
for i = 1:size(Sxx, 1)
    auto_spectrum(i, :) = squeeze(Sxx(i, i, :));
end

% save computed auto spectrum
handles.user_data.eeg_auto_spectrum = auto_spectrum;

% save data
guidata(hObject, handles);


% - update spectrum and connectivity
function update_spectrum_and_connectivity_matrix(hObject, eventdata, handles)
% update the spectrum and connectivity both matrix and plot
% handles = guidata(hObject);
% get the spetrum_matrix type (EEG)
contents = cellstr(get(handles.popupmenu_option_spectrum_eeg,'String'));
type_spectrum = contents{get(handles.popupmenu_option_spectrum_eeg,'Value')};
if strcmp(type_spectrum, 'CrossSpectrum')
    compute_spectrum(hObject, eventdata, handles);
elseif strcmp(type_spectrum, 'AutoSpectrum')
    compute_spectrum(hObject, eventdata, handles);
    compute_eeg_auto_spectrum(hObject, eventdata, handles);
end

handles = guidata(hObject);

% get the spetrum_matrix type (factor)
contents = cellstr(get(handles.popupmenu_option_spectrum_factors,'String'));
type_spectrum = contents{get(handles.popupmenu_option_spectrum_factors,'Value')};
if strcmp(type_spectrum, 'Factor CrossSpectrum')
    compute_factor_cross_spectrum(hObject, eventdata, handles);
elseif strcmp(type_spectrum, 'Factor AutoSpectrum')
    compute_factor_cross_spectrum(hObject, eventdata, handles);
    compute_factor_auto_spectrum(hObject, eventdata, handles);
elseif strcmp(type_spectrum, 'Factor Coherence')
    compute_factor_cross_spectrum(hObject, eventdata, handles);
    compute_factor_coherence(hObject, eventdata, handles);
end


handles = guidata(hObject);

% get the connectivity type
contents = cellstr(get(handles.popupmenu_connectivity_measure,'String'));
type_connectivity = contents{get(handles.popupmenu_connectivity_measure,'Value')};

if strcmp(type_connectivity, 'Coherence')
    %{
        Coherence
        Partial-Coherence
        Block-Coherence
        Partial Block-Coh
        PDC
    %}
    compute_coherence(hObject, eventdata, handles);
elseif strcmp(type_connectivity, 'Partial-Coherence')
    compute_partial_coherence(hObject, eventdata, handles);
elseif strcmp(type_connectivity, 'Block-Coherence')
    compute_block_coherence(hObject, eventdata, handles);
elseif strcmp(type_connectivity, 'Partial Block-Coh')
    compute_partial_block_coherence(hObject, eventdata, handles)
elseif strcmp(type_connectivity, 'PDC')
    compute_PDC(hObject, eventdata, handles);
end

% update data
%guidata(hObject, handles);


function update_spectrum_and_connectivity_plot(hObject, eventdata, handles)
% upate the plots for spectrum and connectivity
handles = guidata(hObject);
% update spectrum plot
plot_eeg_spectrum(hObject, eventdata, handles);
plot_factor_spectrum(hObject, eventdata, handles);
% plot the connectivity
% get the connectivity type
contents = cellstr(get(handles.popupmenu_connectivity_measure,'String'));
type_connectivity = contents{get(handles.popupmenu_connectivity_measure,'Value')};

if strcmp(type_connectivity, 'Coherence')
    %{
        Coherence
        Partial-Coherence
        Block-Coherence
        Partial Block-Coh
        PDC
    %}
    plot_coherence(hObject, eventdata, handles);
elseif strcmp(type_connectivity, 'Partial-Coherence')
    plot_partial_coherence(hObject, eventdata, handles);
elseif strcmp(type_connectivity, 'Block-Coherence')
    plot_block_coherence(hObject, eventdata, handles);
elseif strcmp(type_connectivity, 'Partial Block-Coh')
    plot_partial_block_coherence(hObject, eventdata, handles)
elseif strcmp(type_connectivity, 'Partial Directed Coherence (PDC)')
    plot_PDC(hObject, eventdata, handles);
end

% - compute factor coherence matrix
function compute_factor_coherence(hObject, eventdata, handles)
% compute squared coherence
% save it to user_data.coherence_matrix
handles = guidata(hObject);

% get Sxx
Sxx = handles.user_data.factor_cross_spectrum;
%Sxx(isnan(Sxx)) = 0;
Coh = zeros(size(Sxx));
for i = 1:size(Sxx, 3)
    diag_mat = diag(sqrt(diag(Sxx(:, :, i)).^(-1)));
    Coh(:, :, i) = diag_mat * Sxx(:, :, i) * diag_mat;
end
handles.user_data.factor_coherence = Coh;

% save updates
guidata(hObject, handles);

% display
disp('done computing factor coherence');



% - compute coherence matrix
function compute_coherence(hObject, eventdata, handles)
% compute squared coherence
% save it to user_data.coherence_matrix
handles = guidata(hObject);

% get Sxx
Sxx = handles.user_data.spectrum_matrix;
%Sxx(isnan(Sxx)) = 0;
Coh = zeros(size(Sxx));
for i = 1:size(Sxx, 3)
    diag_mat = diag(sqrt(diag(Sxx(:, :, i)).^(-1)));
    Coh(:, :, i) = diag_mat * Sxx(:, :, i) * diag_mat;
end
handles.user_data.coherence_matrix = Coh;

% save updates
guidata(hObject, handles);

% display
disp('done computing coherence');


% - compute partial coherence
function compute_partial_coherence(hObject, eventdata, handles)
% compute squared coherence
% save it to user_data.coherence_matrix
handles = guidata(hObject);

% get Sxx
Sxx = handles.user_data.spectrum_matrix;
%Sxx(isnan(Sxx)) = 0;
Partial_coh = zeros(size(Sxx));
for i = 1:size(Sxx, 3)
    Sxx_inv = pinv(Sxx(:, :, i));
    %Sxx_inv(isnan(Sxx_inv)) = 0;
    diag_mat = diag(sqrt(diag(Sxx_inv).^(-1)));
    Partial_coh(:, :, i) = - diag_mat * Sxx_inv * diag_mat;
end
handles.user_data.partial_coherence = Partial_coh;

% save updates
guidata(hObject, handles);

% display
disp('done computing partial coherence');

% - compute block coherence matrix
function compute_block_coherence(hObject, eventdata, handles)
% compute squared coherence
% save it to user_data.coherence_matrix
handles = guidata(hObject);

% get Sxx
Sxx = handles.user_data.spectrum_matrix;

% allocate space
n_block = length(handles.user_data.selected_region_names);
n_freq = size(Sxx, 3);
b_coh = zeros(n_block, n_block, n_freq);

% get the index for each block
block_index = handles.user_data.selected_region_start_end;

% compute the block coherence for each block
for indx_freq = 1:n_freq
    for i = 1:n_block
        indx_ii = block_index(i, 1):block_index(i, 2);
        Sii = det(Sxx(indx_ii, indx_ii, indx_freq));
        for j = 1:n_block
            indx_jj = block_index(j, 1):block_index(j, 2);
            Sjj = det(Sxx(indx_jj, indx_jj, indx_freq));
            
            indx_ij = [indx_ii indx_jj];
            Sij = det(Sxx(indx_ij, indx_ij, indx_freq));
            

            b_coh(i, j, indx_freq) = 1 - Sij / (Sii * Sjj);
        end
    end
end

% if not real, report error
if max(abs(imag(b_coh(:)))) > 1e-9
    error('wrong!');
end

% save b_coh
handles.user_data.block_coherence = real(b_coh);

% save updates
guidata(hObject, handles);

% display
disp('done computing block coherence');

% - plot eeg spectrum
function plot_eeg_spectrum(hObject, eventdata, handles)
% get the spetrum_matrix type
contents = cellstr(get(handles.popupmenu_option_spectrum_eeg,'String'));
type_spectrum = contents{get(handles.popupmenu_option_spectrum_eeg,'Value')};
if strcmp(type_spectrum, 'CrossSpectrum')
    plot_eeg_cross_spectrum(hObject, eventdata, handles);
elseif strcmp(type_spectrum, 'AutoSpectrum')
    plot_eeg_auto_spectrum(hObject, eventdata, handles);
end

% - plot factor spectrum
function plot_factor_spectrum(hObject, eventdata, handles)
handles = guidata(hObject);
% get the spetrum_matrix type
contents = cellstr(get(handles.popupmenu_option_spectrum_factors,'String'));
type_spectrum = contents{get(handles.popupmenu_option_spectrum_factors,'Value')};

% all possible options
%{
Factor CrossSpectrum
Factor Coherence
Factor AutoSpectrum
%}

if strcmp(type_spectrum, 'Factor CrossSpectrum')
    plot_factor_cross_spectrum(hObject, eventdata, handles);
elseif strcmp(type_spectrum, 'Factor AutoSpectrum')
    plot_factor_auto_spectrum(hObject, eventdata, handles);
elseif strcmp(type_spectrum, 'Factor Coherence')
    plot_factor_coherence(hObject, eventdata, handles);
end

% - plot factor cross specgrum
function plot_factor_cross_spectrum(hObject, eventdata, handles)

handles = guidata(hObject);

% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_factor_spectrum_plot);
cla;
margin = handles.user_data.margin_width_factors;
mat_w_margin = matrix_add_margin(...
    handles.user_data.factor_cross_spectrum(:, :, frequency_index), ...
    handles.user_data.factor_start_end, ...
    margin);
imagesc(log(abs(mat_w_margin)), [0 15]);
colormap(handles.axes_factor_spectrum_plot, 'jet');
colorbar(handles.axes_factor_spectrum_plot);
axis(handles.axes_factor_spectrum_plot, 'square')
set(gca,'XTickLabel',[], 'YTickLabel', []);
title('Log Cross-Spectrum');


% - plot eeg cross specgrum
function plot_eeg_cross_spectrum(hObject, eventdata, handles)

handles = guidata(hObject);

% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_spectrum_plot);
cla;
margin = handles.user_data.margin_width;
mat_w_margin = matrix_add_margin(...
    handles.user_data.spectrum_matrix(:, :, frequency_index), ...
    handles.user_data.selected_region_start_end, ...
    margin);
imagesc(log(abs(mat_w_margin)), [0 15]);
colormap(handles.axes_spectrum_plot, 'jet');
colorbar(handles.axes_spectrum_plot);
axis(handles.axes_spectrum_plot, 'square')
set(gca,'XTickLabel',[], 'YTickLabel', []);
title('Log Cross-Spectrum');

% -plot factor autospectrum
function plot_factor_auto_spectrum(hObject, eventdata, handles)
handles = guidata(hObject);

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_factor_spectrum_plot);
cla;
margin = handles.user_data.margin_width_factors;
fill_with = 0; % fill gap using -10
mat_w_margin = matrix_add_row_margin(...
    handles.user_data.factor_auto_spectrum, ...
    handles.user_data.factor_start_end, ...
    margin, ...
    fill_with);
mat_w_margin = real(mat_w_margin);
imagesc(log(mat_w_margin(:, 1:50)), [0 15]);
colormap(handles.axes_factor_spectrum_plot, 'jet');
colorbar(handles.axes_factor_spectrum_plot);
axis(handles.axes_factor_spectrum_plot, 'square')
set(gca,'YTickLabel', []);
xlabel('Frequency');
title('Log Auto-Spectrum')


% -plot eeg autospectrum
function plot_eeg_auto_spectrum(hObject, eventdata, handles)
handles = guidata(hObject);

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_spectrum_plot);
cla;
margin = handles.user_data.margin_width;
fill_with = 0; % fill gap using -10
mat_w_margin = matrix_add_row_margin(...
    handles.user_data.eeg_auto_spectrum, ...
    handles.user_data.selected_region_start_end, ...
    margin, ...
    fill_with);
mat_w_margin = real(mat_w_margin);
imagesc(log(mat_w_margin(:, 1:50)), [0 15]);
colormap(handles.axes_spectrum_plot, 'jet');
colorbar(handles.axes_spectrum_plot);
axis(handles.axes_spectrum_plot, 'square')
set(gca,'YTickLabel', []);
xlabel('Frequency');
title('Log Auto-Spectrum')


% - plot factor coherence
function plot_factor_coherence(hObject, eventdata, handles)
% plot coherence matrix on axes_dependence_metric
handles = guidata(hObject);
% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_factor_spectrum_plot);
cla;
margin = handles.user_data.margin_width_factors;
mat_w_margin = matrix_add_margin(...
    handles.user_data.factor_coherence(:, :, frequency_index), ...
    handles.user_data.factor_start_end, ...
    margin);
imagesc(abs(mat_w_margin).^2 , [0 1]);
colormap(handles.axes_factor_spectrum_plot, 'jet');
colorbar(handles.axes_factor_spectrum_plot);
axis(handles.axes_factor_spectrum_plot, 'square')
set(gca,'XTickLabel',[], 'YTickLabel', []);
title('Squared Coherence');


% - plot coherence
function plot_coherence(hObject, eventdata, handles)
% plot coherence matrix on axes_dependence_metric
handles = guidata(hObject);
% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_dependence_metric);
cla;
margin = handles.user_data.margin_width;
mat_w_margin = matrix_add_margin(...
    handles.user_data.coherence_matrix(:, :, frequency_index), ...
    handles.user_data.selected_region_start_end, ...
    margin);
imagesc(abs(mat_w_margin).^2 , [0 1]);
colormap(handles.axes_dependence_metric, 'jet');
colorbar(handles.axes_dependence_metric);
axis(handles.axes_dependence_metric, 'square')
set(gca,'XTickLabel',[], 'YTickLabel', []);
title('Squared Coherence');

% - plot partial coherence
function plot_partial_coherence(hObject, eventdata, handles)
% plot coherence matrix on axes_dependence_metric
handles = guidata(hObject);
% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_dependence_metric);
cla;
margin = handles.user_data.margin_width;
mat_w_margin = matrix_add_margin(...
    handles.user_data.partial_coherence(:, :, frequency_index), ...
    handles.user_data.selected_region_start_end, ...
    margin);
imagesc(abs(mat_w_margin).^2 , ...
    [0 1]);
colormap(handles.axes_dependence_metric, 'jet');
colorbar(handles.axes_dependence_metric);
axis(handles.axes_dependence_metric, 'square')
set(gca,'XTickLabel',[], 'YTickLabel', []);
title('Squared Partial Coherence');


% - plot block coherence
function plot_block_coherence(hObject, eventdata, handles)
% plot coherence matrix on axes_dependence_metric
handles = guidata(hObject);
% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_dependence_metric);
cla;
mat_w_margin = handles.user_data.block_coherence(:, :, frequency_index);
imagesc(mat_w_margin, ...
    [0 1]);
colormap(handles.axes_dependence_metric, 'jet');
colorbar(handles.axes_dependence_metric);
axis(handles.axes_dependence_metric, 'square')
set(gca,'XTickLabel',[], 'YTickLabel', []);
title('Block Coherence');

% - plot pdc (need to be completed)
function plot_PDC(hObject, eventdata, handles)
handles = guidata(hObject);
% get the frequency
slider_value = round(get(handles.slider_get_frequency,'Value'));
frequency_index = slider_value + 1;

% disp(size(handles.user_data.spectrum_matrix));
% plot
axes(handles.axes_dependence_metric);
cla;
title({'PDC function will be available','soon in future versions'});
%colormap(handles.axes_dependence_metric, 'jet');
%colorbar(handles.axes_dependence_metric);
axis(handles.axes_dependence_metric, 'square')
set(gca,'XTickLabel',[], 'YTickLabel', []);

% --- Executes on button press in pushbutton_compute_spectrum.
function pushbutton_compute_spectrum_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_compute_spectrum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% plot factor activity (in order to re compute the factor activity)
handles = guidata(hObject);
plot_factor_activity_on_panel(hObject, eventdata, handles);


% compute the spectrum
update_spectrum_and_connectivity_matrix(hObject, eventdata, handles);
% update the plot
% handles = guidata(hObject);
update_spectrum_and_connectivity_plot(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function axes_spectrum_plot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_spectrum_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_spectrum_plot


% --- Executes on key press with focus on slider_trial_index and none of its controls.
function slider_trial_index_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to slider_trial_index (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on listbox_brain_region and none of its controls.
function listbox_brain_region_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to listbox_brain_region (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu_num_factors.
function popupmenu_num_factors_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_num_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_num_factors contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_num_factors
contents = cellstr(get(hObject,'String'));
num_factors = str2num(contents{get(hObject,'Value')});
handles.user_data.num_factors = num_factors;
guidata(hObject, handles);
plot_factor_activity_on_panel(hObject, eventdata, handles);
pushbutton_compute_spectrum_Callback(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function popupmenu_num_factors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_num_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu_prop_var.
function popupmenu_prop_var_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_prop_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_prop_var contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_prop_var


% --- Executes during object creation, after setting all properties.
function popupmenu_prop_var_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_prop_var (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu10.
function popupmenu10_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu10 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu10


% --- Executes during object creation, after setting all properties.
function popupmenu10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over text_channel_2d_display.
function text_channel_2d_display_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to text_channel_2d_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function axes_dependence_metric_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes_dependence_metric (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes_dependence_metric


% --- Executes on selection change in popupmenu_option_spectrum_factors.
function popupmenu_option_spectrum_factors_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_option_spectrum_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu_option_spectrum_factorssssss contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_option_spectrum_factorssssss
update_spectrum_and_connectivity_matrix(hObject, eventdata, handles);
update_spectrum_and_connectivity_plot(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function popupmenu_option_spectrum_factors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_option_spectrum_factors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
